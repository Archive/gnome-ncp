/* gnome-nwmsgboard - Message board client for Netware
 *
 * Copyright 2003 Ximian, Inc.
 *
 * Author:
 *     Rodrigo Moya <rodrigo@ximian.com>
 */

#include <ncp/ncplib.h>
#include <ncp/nwcalls.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtktable.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkwindow.h>
#include <glade/glade-xml.h>
#include <libgnomeui/gnome-app.h>

static struct ncp_conn *conn;

static void
cmd_quit_cb (GtkObject *object, gpointer user_data)
{
	GtkWidget *main_window = (GtkWidget *) user_data;

	gtk_widget_destroy (main_window);
}

static gboolean
timeout_cb (gpointer user_data)
{
	gchar message[512];
	char sn[256];
	GtkTreeIter iter;
	GtkWidget *msg_list, *main_window = (GtkWidget *) user_data;

	if (ncp_get_broadcast_message (conn, message) != 0) {
		gnome_app_message (GNOME_APP (main_window), "Could not get broadcast message");
		return TRUE;
	}

	if (strlen (message) == 0) {
		gnome_app_message (GNOME_APP (main_window), "No message");
		return TRUE;
	}

	if (NWCCGetConnInfo (conn, NWCC_INFO_SERVER_NAME, sizeof (sn), sn))
		strcpy (sn, "<unknown>");

	/* write the message to the message list */
	msg_list = g_object_get_data (G_OBJECT (main_window), "GNW_MessageList");
	g_assert (msg_list != NULL);

	gtk_list_store_append ((GtkListStore *) gtk_tree_view_get_model (GTK_TREE_VIEW (msg_list)), &iter);
	gtk_list_store_set ((GtkListStore *) gtk_tree_view_get_model (GTK_TREE_VIEW (msg_list)), &iter,
			    0, sn,
			    1, message, -1);

	return TRUE;
}

static GtkWidget *
load_main_window (void)
{
	GladeXML *xml;
	GtkWidget *main_window, *msg_list;
	GtkListStore *model;
	GtkCellRenderer *cell_renderer;
	GtkTreeViewColumn *column;
	gint tid;

	xml = glade_xml_new ("gnome-nwmsgboard.glade", "main-window", NULL);
	if (!xml) {
		/* FIXME: display an error message */
		return;
	}

	/* the main window */
	main_window = glade_xml_get_widget (xml, "main-window");
	g_signal_connect (G_OBJECT (main_window), "delete-event", G_CALLBACK (cmd_quit_cb), main_window);
	g_signal_connect (G_OBJECT (main_window), "destroy", G_CALLBACK (gtk_main_quit), NULL);

	g_object_set_data_full (G_OBJECT (main_window), "GNW_GladeXML", xml, (GDestroyNotify) g_object_unref);

	/* the message list */
	msg_list = glade_xml_get_widget (xml, "message-list");
	model = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	gtk_tree_view_set_model (GTK_TREE_VIEW (msg_list), (GtkTreeModel *) model);
	g_object_unref (model);

	cell_renderer = gtk_cell_renderer_text_new ();
        column = gtk_tree_view_column_new_with_attributes ("From", cell_renderer, "text", 0, NULL);
        gtk_tree_view_column_set_resizable (column, TRUE);
        gtk_tree_view_append_column (GTK_TREE_VIEW (msg_list), column);

	g_object_set_data (G_OBJECT (main_window), "GNW_MessageList", msg_list);

	/* add the timeout for retrieving messages */
	tid = g_timeout_add (2000, (GSourceFunc) timeout_cb, main_window);

	return main_window;
}

int
main (int argc, char *argv[])
{
	long err;
	GtkWidget *window;

	gtk_init (&argc, &argv);

	/* connect to Netware network */
	if ((conn = ncp_initialize (&argc, argv, 1, &err)) == NULL) {
		/* FIXME: display error message */
		g_message ("Could not initialize Netware connection");
		return 1;
	}

	/* create the main window */
	window = load_main_window ();
	gtk_widget_show (window);

	gtk_main ();

	/* free resources */
	ncp_close (conn);

	return 0;
}
