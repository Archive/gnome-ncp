/* ncp-method.h - VFS modules for Netware
 *     
 * Copyright 2003 Ximian, Inc.
 *     
 * Author:
 *     Michael Meeks <michael@ximian.com>
 */

/*
 *
 * TODO:
 *
	+ connection cache to avoid constant re-authentication
	+ package up & commit to CVS ...
	+ implement write, rename etc.
	+ try to use ~/.ncpass(?) file for auth on first try.
	+ seeking: see how the kernel avoids round-trips here.
	+ finish error mapping method ...
	+ work out how caching should work [ performance !].
		+ server not found -> bin toplevel server ?
		+ 'complete' bit - for directory listings.
 */

#include "config.h"
#include "gnome-vfs-extras-i18n.h"
#include <string.h>
#include <errno.h>

#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime.h>

#include <libgnomevfs/gnome-vfs-method.h>
#include <libgnomevfs/gnome-vfs-module.h>
#include <libgnomevfs/gnome-vfs-module-shared.h>
#include <libgnomevfs/gnome-vfs-module-callback-module-api.h>
#include <libgnomevfs/gnome-vfs-standard-callbacks.h>

#include <ncp/ncplib.h>

#define ENABLE_CACHING

/* Make the types more Gtk-ish */
typedef struct ncp_conn               NcpConnection;
typedef struct ncp_volume_list_handle NcpVolume;
typedef struct nw_info_struct2        NcpInfo2;

#define NCP_LOCK()   g_mutex_lock (ncp_lock)
#define NCP_UNLOCK() g_mutex_unlock (ncp_lock)
static GMutex       *ncp_lock = NULL;

#define HANDLE_ERR(err,msg) \
	handle_error (err, msg)

/* Should these be different for nicer icons ? x-directory/ncp ? */
#define NCP_MIME_TYPE_HOST   "x-directory/normal"
#define NCP_MIME_TYPE_VOLUME "x-directory/normal"

static GnomeVFSResult
handle_error (long err, const char *msg)
{
	g_warning ("%s: '%s'", msg, strnwerror (err));
	return GNOME_VFS_ERROR_IO;
}

typedef enum {
	NCP_ROOT,
	NCP_HOST,
	NCP_VOLUME
} NcpType;

typedef struct {
	NcpType  type;
	char    *host;
	char    *volume;
	char    *path;
	char    *username;
	char    *password;
	int      auth_attempts;
} NcpURI;

static const char *
get_next_path_elem (const char *path, char **result)
{
	const char *p, *end;

	*result = NULL;

	if (!path)
		return NULL;

	if (*path == '/')
		path++;

	if (*path == '\0')
		return NULL;

	if (!(p = strchr (path, '/'))) {
		p = path + strlen (path);
		end = NULL;

	} else 
		end = p + 1;

	*result = g_strndup (path, p - path);
	p++;

	return end;
}

static NcpURI *
ncp_uri_new (GnomeVFSURI *uri)
{
	const char *p;
	NcpURI *nuri = g_new0 (NcpURI, 1);

	p = gnome_vfs_uri_get_host_name (uri);
	nuri->host = g_strdup (p);

	p = gnome_vfs_uri_get_path (uri);
	if (!nuri->host)
		p = get_next_path_elem (p, &nuri->host);
	p = get_next_path_elem (p, &nuri->volume);
	nuri->path = g_strdup (p);

	nuri->username = g_strdup (gnome_vfs_uri_get_user_name (uri));
	nuri->password = g_strdup (gnome_vfs_uri_get_password (uri));

	if (!nuri->host && !nuri->volume)
		nuri->type = NCP_ROOT;
	else if (!nuri->volume)
		nuri->type = NCP_HOST;
	else
		nuri->type = NCP_VOLUME;

	g_warning ("ncp uri: %d, '%s' '%s' '%s' '%s' '%s'",
		   nuri->type, nuri->host, nuri->volume, nuri->path,
		   nuri->username, nuri->password);

	return nuri;
}

static void
ncp_uri_free (NcpURI *nuri)
{
	if (nuri) {
		g_free (nuri->host);
		g_free (nuri->volume);
		g_free (nuri->path);
		g_free (nuri->username);
		g_free (nuri->password);
		g_free (nuri);
	}
}

#define NCP_CNX_CLOSE(cnx) /* do nothing, we keep them around indefinately */

typedef struct {
	struct nw_info_struct nwinfo;
	NcpConnection        *cnx;
} NcpNode;

static GNode *node_root = NULL;

static GNode *
node_lookup_T (GNode *node, const char *name)
{
	GNode *l;

	for (l = node->children; l; l = l->next) {
		NcpNode *node = l->data;

		if (!g_strcasecmp (node->nwinfo.entryName, name))
			return l;
	}

	return NULL;
}

static NcpNode *
node_new (const struct nw_info_struct *nwinfo)
{
	NcpNode *node = g_new0 (NcpNode, 1);

	node->nwinfo = *nwinfo;

/*	g_warning ("Node new '%s' -> %d", nwinfo->entryName, nwinfo->dirEntNum); */

	return node;
}

#warning Need to free NcpNodes too & release the cnx. sensibly.

static GNode *
host_lookup_T (const char *name)
{
	GNode *host;
	struct nw_info_struct nwinfo = { 0 };

	if (!(host = node_lookup_T (node_root, name))) {
		strncpy (nwinfo.entryName, name, sizeof (nwinfo.entryName));
		host = g_node_new (node_new (&nwinfo));
		g_node_insert (node_root, 0, host);
	}

	return host;
}

static GnomeVFSResult
ncp_node_lookup (NcpConnection *cnx, NcpURI *nuri, gboolean get_parent, NcpNode **node)
{
	long err;
	GnomeVFSResult result = GNOME_VFS_OK;
	GNode *host, *volume;
	struct nw_info_struct nwinfo = { 0 };

	NCP_LOCK ();

	*node = NULL;
	host = host_lookup_T (nuri->host);

	if (!(volume = node_lookup_T (host, nuri->volume))) {
		if ((err = ncp_do_lookup (cnx, NULL, nuri->volume, &nwinfo))) {
			if (err == NWE_VOL_INVALID) {
				result = GNOME_VFS_ERROR_NOT_FOUND;
				g_warning ("No such volume");
			} else
				result = HANDLE_ERR (err, "vol lookup");
		} else {
			strncpy (nwinfo.entryName, nuri->volume, sizeof (nwinfo.entryName));
			volume = g_node_new (node_new (&nwinfo));
			g_node_insert (host, 0, volume);
		}
	} else
		g_warning ("hit volume cache");

	if (result == GNOME_VFS_OK) {
		int i;
		char **paths;
		NcpNode *un = volume->data;
		GNode   *cur = volume;

		if (nuri->path) {
			paths = g_strsplit (nuri->path, "/", -1);

			for (i = 0; paths && paths[i] && result == GNOME_VFS_OK; i++) {
				GNode *child;

				if (!paths[i] || paths[i][0] == '\0')
					continue;

#ifdef ENABLE_CACHING
				if ((child = node_lookup_T (cur, paths[i]))) {
					g_warning ("Hit cache '%s'", paths [i]);
					cur = child;
					un = cur->data;
				} else
#warning FIXME: cache coherence - needs work here ...
#endif

				if ((err = ncp_obtain_file_or_subdir_info
				     (cnx, NW_NS_DOS, NW_NS_DOS,
				      0xFF, RIM_ALL,
				      un->nwinfo.volNumber, un->nwinfo.dirEntNum, 
				      paths[i],
				      &nwinfo)))
					result = HANDLE_ERR (err, "do lookup");
				else {
					un = node_new (&nwinfo);
					cur = g_node_insert (cur, 0, g_node_new (un));
				}
			}
			g_strfreev (paths);
		}

		if (get_parent)
			*node = cur->parent->data;
		else
			*node = un;
	}

	NCP_UNLOCK ();

	return result;
}

static gboolean
do_is_local (GnomeVFSMethod *method,
	     const GnomeVFSURI *uri)
{
	/* FIXME: this gets invoked thousands of times */
/*	g_warning ("do_is_local(): %s\n",
	gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE)); */

	return FALSE;
}


static GnomeVFSResult
ncp_authenticate (NcpURI *nuri)
{
	GnomeVFSModuleCallbackAuthenticationIn auth_in;
	GnomeVFSModuleCallbackAuthenticationOut auth_out;

/* 	g_warning ("authenticate '%s' '%s' '%s' '%s' ->",
		   nuri->host, nuri->volume, nuri->username, nuri->password); */

	memset (&auth_in, 0, sizeof (auth_in));
	auth_in.uri = g_strdup_printf ("ncp://%s/%s", nuri->host, nuri->volume);
	auth_in.realm = NULL;
	auth_in.previous_attempt_failed = nuri->auth_attempts++ > 0;
	auth_in.auth_type = AuthTypeDigest;

	memset (&auth_out, 0, sizeof (auth_out));

	gnome_vfs_module_callback_invoke
		(GNOME_VFS_MODULE_CALLBACK_AUTHENTICATION,
		 &auth_in, sizeof (auth_in),
		 &auth_out, sizeof (auth_out));

	g_free (nuri->username);
	nuri->username = auth_out.username;
	nuri->password = auth_out.password;

#warning We should cache these per-volume, and/or try to use ~/.ncpassthing as well

/*	g_warning ("authenticate -> '%s' '%s' '%s' '%s'",
		   nuri->host, nuri->volume, nuri->username, nuri->password); */

	g_free (auth_in.uri);

	return nuri->username ? GNOME_VFS_OK : GNOME_VFS_ERROR_LOGIN_FAILED;
}

static GnomeVFSResult
open_uri (NcpURI *nuri, NcpConnection **cnx, gboolean authenticate)
{
	long err;
	GnomeVFSResult result = GNOME_VFS_OK;

	*cnx = NULL;

	switch (nuri->type) {
	case NCP_ROOT:
		if (!(*cnx = ncp_open (NULL, &err)))
			result = HANDLE_ERR (err, "root open");
		break;

	case NCP_HOST:
	case NCP_VOLUME: {
		NcpNode *host = NULL;
		struct ncp_conn_spec spec;

		do {
			g_assert (authenticate); /* otherwise have to have 2 cached cnx's */
			NCP_LOCK ();
			if (!host)
				host = host_lookup_T (nuri->host)->data;
			if ((*cnx = host->cnx))
				result = GNOME_VFS_OK;
			NCP_UNLOCK ();

			if (*cnx)
				break;
			
			if (result != GNOME_VFS_OK &&
			    (result = ncp_authenticate (nuri)) != GNOME_VFS_OK)
				break; /* user cancel */

			if ((err = ncp_find_conn_spec3 (nuri->host,
							nuri->username,
							nuri->password,
							authenticate ? 1 : 0,
							getuid(), /* uid */
							1, /* allow multiple conns */
							&spec)))
				result = HANDLE_ERR (err, "find conn");
			
			else if (!(*cnx = ncp_open (&spec, &err)))
				result = HANDLE_ERR (err, "open cnx");

			else {
				NCP_LOCK ();
				if (host->cnx) { /* beaten to it - very uncommon */
					if (*cnx)
						ncp_close (*cnx);
					*cnx = host->cnx;
				} else
					host->cnx = *cnx;
				NCP_UNLOCK ();

				result = GNOME_VFS_OK;
			}

		} while (result != GNOME_VFS_OK);

		break;
	}
	}

	return result;
}

typedef struct {
	NcpConnection *conn;
	NcpURI        *nuri;
	union {
		struct ncp_bindery_object obj;        /* root */
		NcpVolume                *vol_handle; /* host */
		struct ncp_search_seq     search;     /* volume */
	} d;
} DirectoryHandle;

static GnomeVFSResult
do_close_directory (GnomeVFSMethod *method,
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSContext *context)
{
	DirectoryHandle *dh = (DirectoryHandle *) method_handle;

	if (dh) {
		switch (dh->nuri->type) {
		case NCP_ROOT:
			break;
		case NCP_HOST:
			ncp_volume_list_end (dh->d.vol_handle);
			break;
		case NCP_VOLUME:
			break;
		}
		ncp_uri_free (dh->nuri);
		NCP_CNX_CLOSE (dh->conn);
		g_free (dh);
	}

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_open_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle **method_handle,
		   GnomeVFSURI *uri,
		   GnomeVFSFileInfoOptions options,
		   GnomeVFSContext *context)

{
	long err;
	GnomeVFSResult result = GNOME_VFS_ERROR_INVALID_URI;
	DirectoryHandle *dh;

	dh = g_new0 (DirectoryHandle, 1);
	*method_handle = (GnomeVFSMethodHandle *) dh;

	g_warning ("do_open_directory() %s\n",
		   gnome_vfs_uri_to_string (uri, 0));

	dh->nuri = ncp_uri_new (uri);

	switch (dh->nuri->type) {
	case NCP_ROOT:
		result = open_uri (dh->nuri, &dh->conn, FALSE);
		dh->d.obj.object_id = 0xffffffff;
		break;

	case NCP_HOST: {
		if ((result = open_uri (dh->nuri, &dh->conn, TRUE)) != GNOME_VFS_OK)
			break;
		
		if ((err = ncp_volume_list_init (dh->conn, NW_NS_DOS, 1,
						 &dh->d.vol_handle)))
			result = HANDLE_ERR (err, "vol list init");
		else
			result = GNOME_VFS_OK;	       
		break;
	}
	case NCP_VOLUME: {
		struct nw_info_struct volinfo;

		result = open_uri (dh->nuri, &dh->conn, TRUE);
		if (result != GNOME_VFS_OK)
			break;

		if ((err = ncp_do_lookup (dh->conn, NULL,
					  dh->nuri->volume,
					  &volinfo)))
			result = HANDLE_ERR (err, "volume lookup");

		// Grok for ncp_add_handle_path2 ... (NCP_PATH_STD)
		else if ((err = ncp_initialize_search2 (dh->conn, &volinfo,
							NW_NS_DOS,
							dh->nuri->path,
							NCP_PATH_STD,
							&dh->d.search)))
			result = HANDLE_ERR (err, "search");
		else
			result = GNOME_VFS_OK;

		break;
	}
	}

	if (result != GNOME_VFS_OK) {
		do_close_directory (NULL, *method_handle, NULL);
		*method_handle = NULL;
	}

	return result;
}

static GnomeVFSResult
construct_dir_item (GnomeVFSFileInfo *file_info, long err,
		    const char *name, const char *mime_type)
{
	GnomeVFSResult result = GNOME_VFS_OK;

	if (!err) {
		file_info->name = g_strdup (name);
		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->mime_type = g_strdup (mime_type);
		file_info->valid_fields = ( GNOME_VFS_FILE_INFO_FIELDS_TYPE |
					    GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE );

	} else if (err == NWE_SERVER_FAILURE || /* host eof */
		   err == NWE_SERVER_UNKNOWN) /* root eof */
		result = GNOME_VFS_ERROR_EOF;

	else {
		g_warning ("name: '%s'", name);
		result = HANDLE_ERR (err, "construct_dir_item");
	}

	return result;
}


/* Pinched from the kernel's msdos-fs code. */

/* Linear day numbers of the respective 1sts in non-leap years. */
static int day_n[] =
{0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 0, 0, 0, 0};
/* Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec */

static time_t
ncp_date_dos2unix (unsigned short time, unsigned short date)
{
	int month, year, secs;

	/* first subtract and mask after that... Otherwise, if
	   date == 0, bad things happen */
	month = ((date >> 5) - 1) & 15;
	year = date >> 9;
	secs = (time & 31) * 2 + 60 * ((time >> 5) & 63) + (time >> 11) * 3600 +
		86400 * ((date & 31) - 1 + day_n[month] + (year / 4) + 
		year * 365 - ((year & 3) == 0 && month < 2 ? 1 : 0) + 3653);

	/* days since 1.1.70 plus 80's leap day */

	return secs; /* localized ? */
}

static GnomeVFSResult
info_to_vfs (GnomeVFSFileInfo *file_info, long flags,
	     const struct nw_info_struct *nwinfo)
{
	file_info->name = g_strdup (nwinfo->entryName);
	file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_SIZE;
	file_info->size = nwinfo->dataStreamSize;
	file_info->valid_fields |= ( GNOME_VFS_FILE_INFO_FIELDS_TYPE |
				     GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE );
	if (nwinfo->attributes & SA_SUBDIR_ONLY ) {
		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->mime_type = g_strdup ("x-directory/normal");
	} else {
		file_info->type = GNOME_VFS_FILE_TYPE_REGULAR;
		file_info->mime_type = g_strdup
			(gnome_vfs_mime_type_from_name (file_info->name));
	}
	file_info->size = nwinfo->dataStreamSize;
	file_info->valid_fields |= ( GNOME_VFS_FILE_INFO_FIELDS_MTIME |
				     GNOME_VFS_FILE_INFO_FIELDS_CTIME );

	file_info->mtime = ncp_date_dos2unix (nwinfo->modifyTime, nwinfo->modifyDate);
	file_info->ctime = ncp_date_dos2unix (nwinfo->creationTime, nwinfo->creationDate);
	/* Access rights ? */
/*	g_warning ("Info to vfs: '%s', 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n",
		   nwinfo->entryName, nwinfo->attributes, nwinfo->flags,
		   nwinfo->dirEntNum, nwinfo->DosDirNum, nwinfo->EADataSize); */

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_read_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle *method_handle,
		   GnomeVFSFileInfo *file_info,
		   GnomeVFSContext *context)
{
	long err;
	GnomeVFSResult result = GNOME_VFS_ERROR_IO;
	DirectoryHandle *dh = (DirectoryHandle *) method_handle;

	switch (dh->nuri->type) {
	case NCP_ROOT:
		err = ncp_scan_bindery_object (dh->conn, dh->d.obj.object_id,
					       NCP_BINDERY_FSERVER, "*",
					       &dh->d.obj);
		result = construct_dir_item (file_info, err, 
					     dh->d.obj.object_name, 
					     NCP_MIME_TYPE_HOST);
		break;

	case NCP_HOST: {
		unsigned int vol_num;
		char vol_name[128];
		
		vol_name[0] = '\0';
		err = ncp_volume_list_next (dh->d.vol_handle, &vol_num,
					    vol_name, sizeof (vol_name) -1);

		result = construct_dir_item (file_info, err,
					     vol_name, NCP_MIME_TYPE_VOLUME);
		break;
	}
	case NCP_VOLUME: {
		struct nw_info_struct nwinfo;

		if ((err = ncp_search_for_file_or_subdir (dh->conn,
							  &dh->d.search,
							  &nwinfo))) {
			if (err == NWE_SERVER_FAILURE)
				result = GNOME_VFS_ERROR_EOF;
			else
				result = HANDLE_ERR (err, "search_for_file_or_subdir");
		} else
			result = info_to_vfs (file_info, RIM_ALL, &nwinfo);
		
		break;
	}
	}

	return result;
}

static GnomeVFSResult
do_check_same_fs (GnomeVFSMethod *method,
		  GnomeVFSURI *a,
		  GnomeVFSURI *b,
		  gboolean *same_fs_return,
		  GnomeVFSContext *context)
{
	g_warning ("do_check_same_fs()");
	return FALSE;
}

typedef struct {
	NcpConnection      *cnx;
	NcpURI             *nuri;
	GnomeVFSFileSize    offset;
	struct nw_file_info finfo;
} NcpFileHandle;

static GnomeVFSResult
real_get_file_info (NcpURI                 *nuri,
		    GnomeVFSFileInfo       *file_info,
		    GnomeVFSFileInfoOptions options,
		    GnomeVFSContext        *context)
{
	GnomeVFSResult result = GNOME_VFS_ERROR_NOT_SUPPORTED;

	switch (nuri->type) {
	case NCP_ROOT:
		result = construct_dir_item (file_info, 0, "/", "x-directory/normal");
		break;
	case NCP_HOST: {
		NcpConnection *cnx;

		result = open_uri (nuri, &cnx, TRUE);
		if (result != GNOME_VFS_OK)
			break;

		result = construct_dir_item (file_info, 0, nuri->host,
					     NCP_MIME_TYPE_HOST);

		NCP_CNX_CLOSE (cnx);
		break;
	}
	case NCP_VOLUME: {
		NcpNode *node;
		NcpConnection *cnx;

		result = open_uri (nuri, &cnx, TRUE);
		if (result != GNOME_VFS_OK)
			break;

		if (!(result = ncp_node_lookup (cnx, nuri, FALSE, &node)))
			result = info_to_vfs (file_info, RIM_ALL, &node->nwinfo);

		NCP_CNX_CLOSE (cnx);
		break;
	}
	}
	
	return result;
}

static GnomeVFSResult
do_get_file_info (GnomeVFSMethod *method,
		  GnomeVFSURI *uri,
		  GnomeVFSFileInfo *file_info,
		  GnomeVFSFileInfoOptions options,
		  GnomeVFSContext *context)
{
	NcpURI *nuri = ncp_uri_new (uri);
	GnomeVFSResult result;

	g_warning ("do_get_file_info() %s\n",
		   gnome_vfs_uri_to_string (uri, 0));

	result = real_get_file_info (nuri, file_info, options, context);

	ncp_uri_free (nuri);

	return result;
}

static GnomeVFSResult
do_get_file_info_from_handle (GnomeVFSMethod         *method,
			      GnomeVFSMethodHandle   *method_handle,
			      GnomeVFSFileInfo       *file_info,
			      GnomeVFSFileInfoOptions options,
			      GnomeVFSContext        *context)
{
	NcpFileHandle *mh = (NcpFileHandle *)method_handle;

	return real_get_file_info (mh->nuri, file_info, options, context);
}

static GnomeVFSResult
do_seek (GnomeVFSMethod       *method,
	 GnomeVFSMethodHandle *method_handle,
	 GnomeVFSSeekPosition  whence,
	 GnomeVFSFileOffset    offset,
	 GnomeVFSContext      *context)
{
	GnomeVFSResult result = GNOME_VFS_OK;
	NcpFileHandle *mh = (NcpFileHandle *)method_handle;

	switch (whence) {
	case GNOME_VFS_SEEK_START:
		mh->offset = offset;
		break;
	case GNOME_VFS_SEEK_CURRENT:
		if (offset + mh->offset < 0)
			mh->offset = 0;
		else
			mh->offset += offset;
		break;
	case GNOME_VFS_SEEK_END:
		g_warning ("SEE_END not implemented");
	default:
		result = GNOME_VFS_ERROR_NOT_SUPPORTED;
		break;
	}

	return result;
}

static GnomeVFSResult
do_tell (GnomeVFSMethod       *method,
	 GnomeVFSMethodHandle *method_handle,
	 GnomeVFSFileOffset   *offset_return)
{
	
	NcpFileHandle *mh = (NcpFileHandle *)method_handle;

	*offset_return = mh->offset;

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_close (GnomeVFSMethod *method,
	  GnomeVFSMethodHandle *method_handle,
	  GnomeVFSContext *context)

{
	long err;
	GnomeVFSResult result = GNOME_VFS_OK;
	NcpFileHandle *mh = (NcpFileHandle *) method_handle;

/*	g_warning ("do_close()\n"); */
	
	if (mh->finfo.opened &&
	    (err = ncp_close_file (mh->cnx, mh->finfo.file_handle)))
		result = HANDLE_ERR (err, "close_file");

	if (mh->cnx)
		NCP_CNX_CLOSE (mh->cnx);

	ncp_uri_free (mh->nuri);
	g_free (mh);

	return result;
}

static GnomeVFSResult
do_open (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle **method_handle,
	 GnomeVFSURI *uri,
	 GnomeVFSOpenMode mode,
	 GnomeVFSContext *context)
{
	long err;
	NcpNode *node;
	NcpFileHandle *mh = g_new0 (NcpFileHandle, 1);
	GnomeVFSResult result;

	*method_handle = (GnomeVFSMethodHandle *) mh;

	mh->nuri = ncp_uri_new (uri);


	if (!(result = open_uri (mh->nuri, &mh->cnx, TRUE)) &&
	    !(result = ncp_node_lookup (mh->cnx, mh->nuri, TRUE, &node))) {
		char *fname = strrchr (mh->nuri->path, '/');

		if (!fname)
			fname = mh->nuri->path;
		else
			fname++;

		if ((err = ncp_open_create_file_or_subdir
			(mh->cnx, &node->nwinfo, fname,
			 OC_MODE_OPEN | OC_MODE_OPEN_64BIT_ACCESS, /* open_create_mode */
			 0, /* create attributes */
			 AR_READ_ONLY, /* | AR_WRITE_ONLY */
			 &mh->finfo))) 
			result = HANDLE_ERR (err, "open_file");
		else
			result = GNOME_VFS_OK;
	}

/*	g_warning ("do_open() %s mode %d %p 0x%x%x%x%x%x%x -> %d\n",
		   gnome_vfs_uri_to_string (uri, 0), mode,
		   mh->cnx,
		   mh->finfo.file_handle[0],
		   mh->finfo.file_handle[1],
		   mh->finfo.file_handle[2],
		   mh->finfo.file_handle[3],
		   mh->finfo.file_handle[4],
		   mh->finfo.file_handle[5],
		   result); */

	if (result != GNOME_VFS_OK) {
		do_close (NULL, *method_handle, NULL);
		*method_handle = NULL;
	}

	return result;
}

static GnomeVFSResult
do_read (GnomeVFSMethod       *method,
	 GnomeVFSMethodHandle *method_handle,
	 gpointer              buffer,
	 GnomeVFSFileSize      num_bytes,
	 GnomeVFSFileSize     *bytes_read,
	 GnomeVFSContext      *context)
{
	long err;
	size_t b_read = 0;
	GnomeVFSResult result = GNOME_VFS_OK;
	NcpFileHandle *mh = (NcpFileHandle *)method_handle;

/*	g_warning ("do_read() %Lu bytes %p\n", num_bytes, mh->cnx); */

	if ((err = ncp_read64 (mh->cnx, mh->finfo.file_handle,
			       mh->offset, num_bytes, buffer,
			       &b_read)))
		result = HANDLE_ERR (err, "ncp_read64");
	else
		mh->offset += b_read;

	if (bytes_read)
		*bytes_read = b_read;

	return result;
}


static GnomeVFSMethod method = {
	sizeof (GnomeVFSMethod),
	do_open,
	NULL, /* do_create, */
	do_close,
	do_read,
	NULL, /* do_write, */
	do_seek, /* do_seek, */
	do_tell, /* do_tell, */
	NULL, /* do_truncate_handle, */
	do_open_directory,
	do_close_directory,
	do_read_directory,
	do_get_file_info,
	do_get_file_info_from_handle,
	do_is_local,
	NULL, /* do_make_directory, */
	NULL, /* do_remove_directory, */
	NULL, /* do_move, */
	NULL, /* do_unlink, */
	do_check_same_fs,
	NULL, /* do_set_file_info, */
	NULL, /* do_truncate */
	NULL, /* do_find_directory */
	NULL  /* do_create_symbolic_link */
};

GnomeVFSMethod *
vfs_module_init (const char *method_name, const char *args)
{
	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	ncp_lock = g_mutex_new ();
	node_root = g_node_new (NULL);

	return &method;
}

void
vfs_module_shutdown (GnomeVFSMethod *method)
{
}

